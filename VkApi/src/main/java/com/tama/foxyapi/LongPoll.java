package com.tama.foxyapi;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.AbstractMap;
import java.util.Map;

public class LongPoll {
    private static final String PROPERTY_WAIT = "25";
    private static final String PROPERTY_MODE = "10";
    private static final String PROPERTY_VERSION = "3";
    private VkApi vkApi = VkApi.getInstance();

    private String key;
    private String server;
    private Long ts;
    private Long pts;
    private String longPollServer;

    private Map<String, String> params;

    public LongPoll(Map<String, String> params) {
        this.params = params;
        updateServer();
    }

    public JsonElement getUpdate() {
        String response;
        response = VkApi.getInstance().responseToServerWithParams(longPollServer, null);
        JsonElement element = new JsonParser().parse(response);
        JsonElement tsElement = element.getAsJsonObject().get("ts");
        if (tsElement != null) {
            ts = tsElement.getAsLong();
            updateLongPoll();
        }
        return element;
    }

    public void updateServer() {
        JsonElement method = vkApi.executeMethod(VkMethods.MESSAGES_GET_LONG_POLL.getName(),
                new AbstractMap.SimpleEntry<>("need_pts", params.get("need_pts")),
                new AbstractMap.SimpleEntry<>("lp_version", params.get("lp_version")),
                new AbstractMap.SimpleEntry<>("access_token", vkApi.getSettings().getToken()),
                new AbstractMap.SimpleEntry<>("v", vkApi.getSettings().getVersion()));

        key = method.getAsJsonObject().get("response").getAsJsonObject().get("key").getAsString();
        server = method.getAsJsonObject().get("response").getAsJsonObject().get("server").getAsString();
        ts = method.getAsJsonObject().get("response").getAsJsonObject().get("ts").getAsLong();
        pts = method.getAsJsonObject().get("response").getAsJsonObject().get("pts").getAsLong();

        updateLongPoll();

        System.out.println(longPollServer);
    }

    private void updateLongPoll() {
        longPollServer = String.format("https://%s?act=a_check&key=%s&ts=%d&wait=%s&mode=%s&version=%s",
                server, key, ts, PROPERTY_WAIT, PROPERTY_MODE, PROPERTY_VERSION);
    }
}
