package com.tama.foxyapi

data class LongPollEventContainer(val type: Long,
                                  val idMessage: Long,
                                  val flags: Long,
                                  val idChar: Long,
                                  val unixTime: Long,
                                  val text: String,
                                  val attachment: String)