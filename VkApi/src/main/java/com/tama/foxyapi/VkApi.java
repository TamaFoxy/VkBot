package com.tama.foxyapi;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.tama.foxyapi.exception.IOApiException;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.*;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class VkApi {

    private static final String URL_VK_API_METHOD = "https://api.vk.com/method/";
    private static final String RESOURCES_DIR = "resources" + File.separator;
    private static final String SETTINGS_FILE_NAME = RESOURCES_DIR + "Settings.json";

    private static String SPACE;

    private Settings settings;

    private static VkApi instance;

    private VkApi() {
        try {
            new File(RESOURCES_DIR).mkdir();
            settings = fromJson(SETTINGS_FILE_NAME, Settings.class);
        } catch (IOApiException e) {
            System.out.println("CREATE SETITNGS JSON");
            settings = new Settings();
            settings.setOwnerId("1");
            settings.setToken("1");
            settings.setVersion("1");
            toJson(SETTINGS_FILE_NAME, settings);
        }
    }

    public static VkApi getInstance() {
        if (instance == null) {
            synchronized (VkApi.class) {
                if (instance == null) {
                    instance = new VkApi();
                }
            }
        }

        return instance;
    }

    @SafeVarargs
    public final JsonElement executeMethod(String method, Map.Entry<String, String>... params) {
        Map<String, String> methodParams = new HashMap<>();
        for (Map.Entry<String, String> param : params) {
            methodParams.put(param.getKey(), param.getValue());
        }

        String apiMethod = formatToApiMethod(method, methodParams);
        String get = responseToServerWithParams(apiMethod, null);
        return new JsonParser().parse(get);
    }

    public void sendMessage(Long idChat, String text) {
        try {
            text = URLEncoder.encode(text, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException();
        }
        URI uri = URI.create(text);
        executeMethod(VkMethods.MESSAGES_SEND.getName(),
                Map.entry("peer_id", String.valueOf(idChat)),
                Map.entry("message", uri.toASCIIString()),
                Map.entry("access_token", VkApi.getInstance().getSettings().getToken()),
                Map.entry("v", VkApi.getInstance().getSettings().getVersion())
        );

    }

    public void sendAttachmentToChat(Long idChat, String attachment) {
        executeMethod(VkMethods.MESSAGES_SEND.getName(),
                Map.entry("peer_id", String.valueOf(idChat)),
                Map.entry("attachment", attachment),
                Map.entry("access_token", VkApi.getInstance().getSettings().getToken()),
                Map.entry("v", VkApi.getInstance().getSettings().getVersion())
        );

    }

    private String formatToApiMethod(String methodName, Map<String, String> params) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(URL_VK_API_METHOD).append(methodName).append("?");

        for (String param : params.keySet()) {
            stringBuilder.append(param).append("=").append(params.get(param)).append("&");
        }

        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        return stringBuilder.toString();
    }

    public String responseToServerWithParams(String url, Map<String, String> params) {
        HttpGet httpGet = new HttpGet(url);
        ResponseHandler<String> handler = httpResponse -> {
            HttpEntity entity = httpResponse.getEntity();
            return entity != null ? EntityUtils.toString(entity) : null;
        };

        String responseBody = null;
        try {
            HttpClient httpClient = HttpClientBuilder.create()
                    .build();
            responseBody = httpClient.execute(httpGet, handler);
        } catch (IOException e) {
            e.printStackTrace();
        }

        httpGet.abort();
        return responseBody;
    }

    public <T> void toJson(String fileName, T obj) {
        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(fileName))) {
            Gson gson = new Gson();
            String jsonString = gson.toJson(obj);
            writer.write(jsonString);
        } catch (IOException e) {
            throw new IOApiException("Файл " + fileName + " не записан");
        }
    }

    public <T> T fromJson(String fileName, Class<T> clazz) {
        try {
            Gson gson = new Gson();
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), StandardCharsets.UTF_8));
            return gson.fromJson(br, clazz);
        } catch (FileNotFoundException e) {
            throw new IOApiException("Файл " + fileName + " не найден");
        }
    }

    public <T> T fromJson(String fileName, Type type) {
        try {
            Gson gson = new Gson();
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), StandardCharsets.UTF_8));
            return gson.fromJson(br, type);
        } catch (FileNotFoundException e) {
            throw new IOApiException("Файл " + fileName + " не найден");
        }
    }

    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    public boolean isConference(Long chatId) {
        return chatId - 2000000000 > 0;
    }

}
