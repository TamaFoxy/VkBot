package com.tama.foxyapi;

public enum  VkMethods {
    PHOTOS_GET_ALBOMS("photos.getAlbums"),
    PHOTOS_GET("photos.get"),
    MESSAGES_GET_LONG_POLL("messages.getLongPollServer"),
    MESSAGES_SEND("messages.send"),
    MESSAGES_GET_CHAT("messages.getChat");

    VkMethods(String name) {
        this.name = name;
    }

    private String name;

    public String getName() {
        return name;
    }
}
