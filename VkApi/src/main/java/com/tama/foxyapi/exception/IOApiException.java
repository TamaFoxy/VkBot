package com.tama.foxyapi.exception;

public class IOApiException extends RuntimeException {
    public IOApiException(String message) {
        super(message);
    }
}
