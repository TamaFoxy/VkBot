package com.tama.foxyapi.service

import com.google.gson.JsonElement
import com.tama.foxyapi.LongPollEventContainer

interface ContainerService {
    fun createContainerFromJson(json: JsonElement): LongPollEventContainer
}