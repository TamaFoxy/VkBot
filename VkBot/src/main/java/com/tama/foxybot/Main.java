package com.tama.foxybot;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.tama.foxyapi.LongPoll;
import com.tama.foxyapi.VkApi;
import com.tama.foxybot.commands.*;
import com.tama.foxybot.helpers.LoggerCommandHelper;
import com.tama.foxybot.model.Chat;
import com.tama.foxybot.model.Message;

import java.util.Map;

public class Main {

    private static final int NEW_MESSAGE = 4;
    private static final int UPDATE_MESSAGE = 5;
    private static final int DELETE_MESSAGE = 2;

    //ToDo говно. Переписать.
    public static void main(String[] args) {
        VkBot bot = new VkBot();
        BaseCommand updatePhoto = CommandFactory.getCommand("!updatePhoto");
        updatePhoto.execute(null);
        System.out.println(VkBot.getPhotoToCommand());
        LongPoll longPoll = new LongPoll(Map.of(
                "need_pts", "1",
                "lp_version", "3"
        ));
        LoggerCommandHelper helper = new LoggerCommandHelper();
        while (true) {
            try {
                Thread.sleep(200L);
                JsonElement update = longPoll.getUpdate();
                JsonElement failed = update.getAsJsonObject().get("failed");
                if (failed != null) {
                    int failedCode = failed.getAsInt();
                    switch (failedCode) {
                        case 2:
                        case 3:
                            System.out.println("ERROR " + failedCode);
                            longPoll.updateServer();
                            break;
                    }
                    continue;
                }
                JsonArray updates = update.getAsJsonObject().get("updates").getAsJsonArray();
                for (JsonElement updateElement : updates) {
                    int type = updateElement.getAsJsonArray().get(0).getAsInt();
                    String commandName = LoggerCommandType.getCommandNameByLongPoll(type);
                    BaseCommand loggerCommand = CommandFactory.getCommand(commandName);
                    if (loggerCommand == null) {
                        continue;
                    }
                    switch (type) {
                        case NEW_MESSAGE:
                            loggerCommand.execute(updateElement.getAsJsonArray());
                            System.out.println(updateElement);
                            Message message = helper.getMessage(updateElement.getAsJsonArray());
                            Chat chat = helper.getChat(updateElement.getAsJsonArray());
                            String[] words = message.getMessageText().split(" ");
                            if (words.length != 0) {
                                BaseCommand command = CommandFactory.getCommand(words[0]);
                                if (command == null) {
                                    continue;
                                }
                                //ToDo зарефакторить определение команд и назначения.
                                if (!command.isActive()) {
                                    continue;
                                }
                                if (command instanceof ConferenceCommand) {
                                    if (VkApi.getInstance().isConference(chat.getId())) {
                                        command.execute(updateElement.getAsJsonArray());
                                    }
                                    continue;
                                }
                                if (command instanceof UserCommand) {
                                    if (!VkApi.getInstance().isConference(chat.getId())) {
                                        command.execute(updateElement.getAsJsonArray());
                                    }
                                    continue;
                                }
                                command.execute(updateElement.getAsJsonArray());

                            }
                            break;
                        case UPDATE_MESSAGE:
                            loggerCommand.execute(updateElement.getAsJsonArray());
                            System.out.println(updateElement);
                            break;
                        case DELETE_MESSAGE:
                            loggerCommand.execute(updateElement.getAsJsonArray());
                            System.out.println(updateElement);
                            break;
                    }
                }
                VkApi.getInstance().toJson(VkBot.CHATS_FILE_NAME, VkBot.getChatList());
            } catch (RuntimeException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
