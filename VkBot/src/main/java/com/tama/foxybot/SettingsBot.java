package com.tama.foxybot;

public class SettingsBot {
    private String prefixBot;
    private String albomOfRespect;
    private String photoCommands;

    public String getAlbomOfMessagesPhoto() {
        return albomOfRespect;
    }

    public void setAlbomOfRespect(String albomOfRespect) {
        this.albomOfRespect = albomOfRespect;
    }

    public String getPrefixBot() {
        return prefixBot;
    }

    public void setPrefixBot(String prefixBot) {
        this.prefixBot = prefixBot;
    }

    public String getPhotoCommands() {
        return photoCommands;
    }

    public void setPhotoCommands(String photoCommands) {
        this.photoCommands = photoCommands;
    }
}
