package com.tama.foxybot;

import com.google.gson.reflect.TypeToken;
import com.tama.foxyapi.VkApi;
import com.tama.foxyapi.exception.IOApiException;
import com.tama.foxybot.model.Chat;

import java.io.File;
import java.lang.reflect.Type;
import java.util.*;

public class VkBot {

    private static VkApi vkApi = VkApi.getInstance();

    private static Map<Long, Chat> chatList = new HashMap<>();

    private static List<String> photosCommand = new ArrayList<>();
    private static Map<String, List<String>> photoToCommand = new HashMap<>();
    private static SettingsBot settingsBot;

    private static final String RESOURCES_DIR = "resources" + File.separator;
    private static final String SETTINGS_BOT_FILE_NAME = RESOURCES_DIR + "SettingsBot.json";
    public static final String CHATS_FILE_NAME = RESOURCES_DIR + "Chats.json";

    public VkBot() {
        try {
            new File(RESOURCES_DIR).mkdir();
            settingsBot = load(SETTINGS_BOT_FILE_NAME, SettingsBot.class);
            photosCommand.addAll(Arrays.asList(settingsBot.getPhotoCommands().split(";")));
        } catch (IOApiException e) {
            System.out.println("CREATE SETITNGS-BOT JSON");
            settingsBot = new SettingsBot();
            settingsBot.setPrefixBot("1");
            settingsBot.setAlbomOfRespect("1");
            settingsBot.setPhotoCommands("F");
            save(SETTINGS_BOT_FILE_NAME, settingsBot);
        }

        try {
            chatList = load(CHATS_FILE_NAME, new TypeToken<Map<Long, Chat>>() {
            }.getType());
        } catch (IOApiException e) {
            System.out.println("CREATE CHAT JSON");
            chatList = new HashMap<>();
            save(CHATS_FILE_NAME, chatList);
        }
    }

    private static  <T> T load(String filename, Class<T> objClass) {
        return vkApi.fromJson(filename, objClass);
    }

    private static  <T> T load(String filename, Type objType) {
        return vkApi.fromJson(filename, objType);
    }

    private static  <T> void save(String filename, T object) {
        vkApi.toJson(filename, object);
    }

    public static SettingsBot getSettingsBot() {
        return settingsBot;
    }

    public static Map<Long, Chat> getChatList() {
        return chatList;
    }

    public static Map<String, List<String>> getPhotoToCommand() {
        return photoToCommand;
    }

    public static List<String> getPhotosCommand() {
        return photosCommand;
    }

    public static void updateSettings(SettingsBot settingsBot) {
        save(SETTINGS_BOT_FILE_NAME, settingsBot);
    }
}
