package com.tama.foxybot.commands;

import com.google.gson.JsonArray;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseCommand {
    protected boolean isActive = true;

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public abstract String getCommandName();

    public List<String> getCommandNameList() {
        return new ArrayList<>();
    }

    public abstract void execute(JsonArray params);
}
