package com.tama.foxybot.commands;

import org.reflections.Reflections;

import java.lang.reflect.InvocationTargetException;
import java.util.*;

public class CommandFactory {
    private static Map<String, BaseCommand> commandMap = new HashMap<>();
    private static Map<Class, BaseCommand> commandList = new HashMap<>();

    static {
        try {
            Reflections reflections = new Reflections("com.tama.foxybot.commands.impl");
            Set<Class<? extends BaseCommand>> classes = reflections.getSubTypesOf(BaseCommand.class);
            for (Class<? extends BaseCommand> commandClass : classes) {
                BaseCommand baseCommand = commandClass.getConstructor(null).newInstance();
                commandList.put(commandClass, baseCommand);
                String commandName = baseCommand.getCommandName();
                if (commandName != null) {
                    commandMap.put(commandName, baseCommand);
                }
                List<String> commandNameList = baseCommand.getCommandNameList();
                if (commandNameList != null && !commandNameList.isEmpty()) {
                    for (String name : commandNameList) {
                        commandMap.put(name, baseCommand);
                    }
                }
                if (baseCommand.getClass().isAssignableFrom(LoggerCommand.class)) {
                    baseCommand.setActive(true);
                }
            }
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
    }


    public static BaseCommand getCommand(String command) {
        return commandMap.get(command);
    }

    public static void setActive(String command, boolean active) {
        BaseCommand baseCommand = commandMap.get(command);
        if (baseCommand != null) {
            baseCommand.setActive(active);
        }
    }

    public static <T> void update(Class<T> clazz) {
        BaseCommand baseCommand = commandList.get(clazz);
        String commandName = baseCommand.getCommandName();
        if (commandName != null) {
            commandMap.put(commandName, baseCommand);
        }
        List<String> commandNameList = baseCommand.getCommandNameList();
        if (commandNameList != null && !commandNameList.isEmpty()) {
            for (String name : commandNameList) {
                commandMap.put(name, baseCommand);
            }
        }
    }

    public static void remove(String command) {
        commandMap.remove(command);
    }
}
