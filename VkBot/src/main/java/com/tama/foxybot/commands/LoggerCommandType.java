package com.tama.foxybot.commands;

public enum LoggerCommandType {
    NEW_MESSAGE("NewMessage", 4),
    UPDATE_MESSAGE("UpdateMessage", 5),
    DELETE_MESSAGE("DeleteMessage", 2);

    private String name;
    private Integer longPollType;

    LoggerCommandType(String name, Integer longPollType) {
        this.name = name;
        this.longPollType = longPollType;
    }

    public Integer getLongPollType() {
        return longPollType;
    }

    public static String getCommandNameByLongPoll(Integer longPoll) {
        for (LoggerCommandType loggerCommandType : values()) {
            if (loggerCommandType.longPollType.equals(longPoll)) {
                return loggerCommandType.name;
            }
        }

        return null;
        //ToDo возможно после рефакторинга ошибок нужно будет раскомментировать и отлавливать
//        throw new RuntimeBotException("Не найдена команда по longPoll значению");
    }
}
