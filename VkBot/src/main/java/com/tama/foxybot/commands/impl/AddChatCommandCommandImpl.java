package com.tama.foxybot.commands.impl;

import com.google.gson.JsonArray;
import com.tama.foxyapi.VkApi;
import com.tama.foxybot.SettingsBot;
import com.tama.foxybot.VkBot;
import com.tama.foxybot.commands.BaseCommand;
import com.tama.foxybot.commands.CommandFactory;
import com.tama.foxybot.commands.UserCommand;
import com.tama.foxybot.helpers.LoggerCommandHelper;
import com.tama.foxybot.model.Chat;
import com.tama.foxybot.model.Message;

public class AddChatCommandCommandImpl extends BaseCommand implements UserCommand {

    private LoggerCommandHelper helper = new LoggerCommandHelper();

    @Override
    public String getCommandName() {
        return "!add";
    }

    @Override
    public void execute(JsonArray params) {
        Chat chat = helper.createChat(params);
        Message message = helper.createMessage(params);

        String[] words = message.getMessageText().split(" ");
        if (words.length != 2) return;

        String newChatCommand = words[1];
        if (newChatCommand.contains(";")) {
            VkApi.getInstance().sendMessage(chat.getId(), "Команда не может содержать символ \";\"");
            return;
        }
        StringBuilder sb = new StringBuilder(VkBot.getSettingsBot().getPhotoCommands());
        int lastSeparator = sb.lastIndexOf(";");
        if (lastSeparator == sb.length()) {
            sb.append(newChatCommand);
        } else {
            sb.append(";").append(newChatCommand);
        }

        VkBot.getSettingsBot().setPhotoCommands(sb.toString());
        VkBot.updateSettings(VkBot.getSettingsBot());

        VkBot.getPhotosCommand().add(newChatCommand);
        UpdatePhotoListCommandImpl command = new UpdatePhotoListCommandImpl();
        command.execute(null);
        CommandFactory.update(ChatToPhotoCommandImpl.class);
    }
}
