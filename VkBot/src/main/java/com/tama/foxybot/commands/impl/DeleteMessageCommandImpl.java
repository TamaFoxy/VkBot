package com.tama.foxybot.commands.impl;

import com.google.gson.JsonArray;
import com.tama.foxyapi.VkApi;
import com.tama.foxybot.commands.BaseCommand;
import com.tama.foxybot.commands.LoggerCommand;
import com.tama.foxybot.exceptions.LoggerUpdateException;
import com.tama.foxybot.helpers.LoggerCommandHelper;
import com.tama.foxybot.model.Chat;
import com.tama.foxybot.model.Message;

public class DeleteMessageCommandImpl extends BaseCommand implements LoggerCommand {

    private LoggerCommandHelper loggerHelper = new LoggerCommandHelper();

    @Override
    public String getCommandName() {
        return "DeleteMessage";
    }

    @Override
    public void execute(JsonArray params) {
        Chat chat = loggerHelper.getChat(params);
        if (chat == null) {
            throw new LoggerUpdateException(String.format("Чат для параметров: %s не найден", params));
        }
        Message message = loggerHelper.getMessage(params);
        if (message == null) {
            throw new LoggerUpdateException(String.format("Сообщение для параметров: %s не найдено", params));
        }
        message.setDeleted(true);

        VkApi.getInstance().sendMessage(chat.getId(),
                "@id" + message.getIdUser() +"(Оно) удалило сообщение - " + message.getMessageText());
    }
}
