package com.tama.foxybot.commands.impl;

import com.google.gson.JsonArray;
import com.tama.foxyapi.VkApi;
import com.tama.foxybot.commands.BaseCommand;
import com.tama.foxybot.commands.ConferenceCommand;
import com.tama.foxybot.helpers.LoggerCommandHelper;
import com.tama.foxybot.model.Chat;
import com.tama.foxybot.model.Message;

import java.util.Date;
import java.util.HashMap;

public class MmmChatCommandImpl extends BaseCommand implements ConferenceCommand {

    private static LoggerCommandHelper helper = new LoggerCommandHelper();
    private static final int FOUR_SECOND = 4_000;
    private HashMap<Long, HashMap<String, Date>> commandRestriction = new HashMap<>();



    @Override
    public String getCommandName() {
        return "Ммм";
    }

    @Override
    public void execute(JsonArray params) {
        String attach = "doc499895504_472119962";
        Chat chat = helper.getChat(params);
        Message message = helper.createMessage(params);
        String commandText = message.getMessageText().split(" ")[0];

        Long idUser = message.getIdUser();
        if (!commandRestriction.containsKey(idUser)) {
            commandRestriction.put(idUser, new HashMap<>());
        }
        HashMap<String, Date> userRestriction = commandRestriction.get(idUser);
        if (!userRestriction.containsKey(commandText)) {
            userRestriction.put(commandText, new Date(0L));
        }
        Date lastCommandUsed = userRestriction.get(commandText);

        if (new Date().getTime() - lastCommandUsed.getTime() > FOUR_SECOND) {
            VkApi.getInstance().sendAttachmentToChat(chat.getId(), attach);
            userRestriction.put(commandText, new Date());
        }
    }
}
