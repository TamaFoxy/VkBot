package com.tama.foxybot.commands.impl;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.tama.foxybot.commands.BaseCommand;
import com.tama.foxybot.commands.LoggerCommand;
import com.tama.foxybot.helpers.LoggerCommandHelper;
import com.tama.foxybot.model.Chat;
import com.tama.foxybot.model.Message;

public class NewMessageCommandImpl extends BaseCommand implements LoggerCommand {

    private LoggerCommandHelper loggerHelper = new LoggerCommandHelper();
    private static final int MESSAGES_ATTACHMENT_INDEX = 6;
    private static final String CHAT_INVITE_USER_EVENT = "chat_invite_user";
    private static final String CHAT_KICK_USER_EVENT = "chat_kick_user";

    @Override
    public String getCommandName() {
        return "NewMessage";
    }

    @Override
    public void execute(JsonArray params) {
        Chat chat = loggerHelper.getChat(params);
        if (chat == null) {
            chat = loggerHelper.createChat(params);
        }
        Message message = loggerHelper.getMessage(params);
        if (message == null) {
            message = loggerHelper.createMessage(params);
        }
        chat.getMessages().put(message.getIdMessage(), message);

        if (isMoveEvent(params)) {
            updateChatInformation(params);
        }
    }

    /**
     * Если это сообщение с уведомлением о том, что человек кикнут\вышел из конференции или наоборот, добавили\вернулся, то вернёт true, иначе false.
     */
    private boolean isMoveEvent(JsonArray params) {
        JsonElement sourceAct = params.get(MESSAGES_ATTACHMENT_INDEX).getAsJsonObject().get("source_act");
        return sourceAct != null;
    }

    private void updateChatInformation(JsonArray params) {
        //ToDo переделать логику работы параметрами params. Возможно вынести в отдельный контейнер, который будет хранить ВСЮ информацию.
        // скорее всего он будет содержать все поля params и json строку attachment. Абстрагироваться от params gson, ввести свою удобную прослойку.
        Chat chat = loggerHelper.getChat(params);
        if (chat == null) {
            //ToDo сделать адекватную обработку ошибок и логику с перехваткой exception на разных уровнях.
            System.out.println("ERROR - Не смог найтись чат");
            return;
        }
        Message message = loggerHelper.createMessage(params);

        JsonElement sourceAct = params.get(MESSAGES_ATTACHMENT_INDEX).getAsJsonObject().get("source_act");
        if (sourceAct == null) return;
        String sourceActText = sourceAct.getAsString();
        Long idUser = message.getIdUser();
        switch (sourceActText) {
            case CHAT_INVITE_USER_EVENT:
                if (chat.getUserIdList().contains(idUser)) return;
                chat.getUserIdList().add(idUser);
                break;
            case CHAT_KICK_USER_EVENT:
                if(!chat.getUserIdList().contains(idUser)) return;
                chat.getUserIdList().remove(idUser);
                break;
            default:
                System.out.println("WOW. Неопознанный тип sourceAct");
        }
    }
}
