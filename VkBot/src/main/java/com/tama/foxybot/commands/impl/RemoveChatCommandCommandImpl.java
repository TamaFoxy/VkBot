package com.tama.foxybot.commands.impl;

import com.google.gson.JsonArray;
import com.tama.foxybot.VkBot;
import com.tama.foxybot.commands.BaseCommand;
import com.tama.foxybot.commands.CommandFactory;
import com.tama.foxybot.commands.UserCommand;
import com.tama.foxybot.helpers.LoggerCommandHelper;
import com.tama.foxybot.model.Message;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RemoveChatCommandCommandImpl extends BaseCommand implements UserCommand {

    private LoggerCommandHelper helper = new LoggerCommandHelper();

    @Override
    public String getCommandName() {
        return "!remove";
    }

    @Override
    public void execute(JsonArray params) {
        Message message = helper.createMessage(params);

        String[] words = message.getMessageText().split(" ");
        if (words.length != 2) return;

        String chatCommand = words[1];
        String commandsAfterRemove = commandsAfterRemove(chatCommand);

        VkBot.getSettingsBot().setPhotoCommands(commandsAfterRemove);
        VkBot.updateSettings(VkBot.getSettingsBot());
        VkBot.getPhotosCommand().remove(chatCommand);
        CommandFactory.remove(chatCommand);
    }

    private String commandsAfterRemove(String commandToRemove) {
        String commands = VkBot.getSettingsBot().getPhotoCommands();


        String betweenCommandRegex = ";" + commandToRemove + ";";
        boolean commandBetweenOther = Pattern.compile(betweenCommandRegex).matcher(commands).find();
        if (commandBetweenOther) {
            return commands.replaceAll(";" + commandToRemove, "");
        }

        String lastCommandRegex = ";" + commandToRemove + "$";
        boolean lastCommand = Pattern.compile(lastCommandRegex).matcher(commands).find();
        if (lastCommand) {
            return commands.replaceAll(lastCommandRegex, "");
        }

        String firstCommandRegex = "^" + commandToRemove + ";";
        boolean firstCommand = Pattern.compile(firstCommandRegex).matcher(commands).find();
        if (firstCommand) {
            return commands.replaceAll(firstCommandRegex, "");
        }

        return commands.replaceAll("^" + commandToRemove + "$", "");
    }
}
