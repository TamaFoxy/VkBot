package com.tama.foxybot.commands.impl;

import com.google.gson.JsonArray;
import com.tama.foxyapi.VkApi;
import com.tama.foxybot.commands.BaseCommand;
import com.tama.foxybot.commands.ConferenceCommand;
import com.tama.foxybot.helpers.LoggerCommandHelper;
import com.tama.foxybot.model.Chat;
import com.tama.foxybot.model.Message;

public class RussianRouletteCommandImpl extends BaseCommand implements ConferenceCommand {

    private VkApi vkApi = VkApi.getInstance();
    private LoggerCommandHelper helper = new LoggerCommandHelper();

    public RussianRouletteCommandImpl() {
        isActive = false;
    }

    @Override
    public String getCommandName() {
        return "!рулетка";
    }

    @Override
    public void execute(JsonArray params) {
        Chat chat = helper.createChat(params);
        Message message = helper.createMessage(params);
        int rand = (Math.random() < 0.95) ? 0 : 1;
        switch (rand) {
            case 1:
                vkApi.sendMessage(chat.getId(), " Оппа! Подбит. @id" + message.getIdUser() + "(Ты) теперь труп.");
                break;
            case 0:
                vkApi.sendMessage(chat.getId(), "Хм. Осечка. Живи, @id" + message.getIdUser() + "(салага).");
                break;

        }
    }
}