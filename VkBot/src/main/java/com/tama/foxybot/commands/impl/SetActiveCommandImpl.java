package com.tama.foxybot.commands.impl;

import com.google.gson.JsonArray;
import com.tama.foxybot.commands.BaseCommand;
import com.tama.foxybot.commands.CommandFactory;
import com.tama.foxybot.commands.UserCommand;
import com.tama.foxybot.helpers.LoggerCommandHelper;
import com.tama.foxybot.model.Message;

import java.util.List;

public class SetActiveCommandImpl extends BaseCommand implements UserCommand {
    @Override
    public String getCommandName() {
        return "!setActive";
    }

    public SetActiveCommandImpl() {
        isActive = true;
    }

    @Override
    public void execute(JsonArray params) {
        Message message = new LoggerCommandHelper().createMessage(params);
        List<String> words = List.of(message.getMessageText().split(" "));
        if (words.size() != 3) {
            return;
        }
        try {
            String command = words.get(1);
            String active = words.get(2);

            CommandFactory.setActive(command, Boolean.valueOf(active));
        } catch (Exception ignored) {

        }
    }
}
