package com.tama.foxybot.commands.impl;

import com.google.gson.JsonArray;
import com.tama.foxyapi.VkApi;
import com.tama.foxybot.VkBot;
import com.tama.foxybot.commands.BaseCommand;
import com.tama.foxybot.commands.UserCommand;
import com.tama.foxybot.helpers.LoggerCommandHelper;
import com.tama.foxybot.model.Chat;
import com.tama.foxybot.model.Message;

public class ShowChatCommandCommandImpl extends BaseCommand implements UserCommand {

    private LoggerCommandHelper helper = new LoggerCommandHelper();

    @Override
    public String getCommandName() {
        return "!show";
    }

    @Override
    public void execute(JsonArray params) {
        Chat chat = helper.getChat(params);

        VkApi.getInstance().sendMessage(chat.getId(), VkBot.getSettingsBot().getPhotoCommands());
    }
}
