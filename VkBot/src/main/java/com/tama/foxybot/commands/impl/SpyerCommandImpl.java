package com.tama.foxybot.commands.impl;

import com.google.gson.JsonArray;
import com.tama.foxyapi.VkApi;
import com.tama.foxybot.commands.BaseCommand;
import com.tama.foxybot.commands.ConferenceCommand;
import com.tama.foxybot.helpers.LoggerCommandHelper;
import com.tama.foxybot.model.Chat;
import com.tama.foxybot.model.Message;

import java.util.List;
import java.util.Random;

public class SpyerCommandImpl extends BaseCommand implements ConferenceCommand {
    LoggerCommandHelper helper = new LoggerCommandHelper();

    List<String> responses = List.of("Держите @id%s(его)!", "@id%s(ШПИОН) КОТА!!", "@id%s(шпион)? Хаха. Это быть шпионом не может.");


    @Override
    public String getCommandName() {
        return "!шпион";
    }

    @Override
    public void execute(JsonArray params) {
        Chat chat = helper.getChat(params);
        Message message = helper.createMessage(params);

        VkApi.getInstance().sendMessage(chat.getId(), String.format(getRandomResponse(), message.getIdUser()));
    }

    private String getRandomResponse() {
        return responses.get(Math.abs(new Random().nextInt()) % responses.size());
    }
}
