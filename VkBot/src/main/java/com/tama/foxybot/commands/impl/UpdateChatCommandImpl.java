package com.tama.foxybot.commands.impl;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.tama.foxyapi.VkApi;
import com.tama.foxyapi.VkMethods;
import com.tama.foxybot.commands.BaseCommand;
import com.tama.foxybot.commands.UserCommand;
import com.tama.foxybot.helpers.LoggerCommandHelper;
import com.tama.foxybot.model.Chat;
import com.tama.foxybot.model.Message;

import java.util.Map;

public class UpdateChatCommandImpl extends BaseCommand implements UserCommand {
    private LoggerCommandHelper loggerHelper = new LoggerCommandHelper();
    private VkApi vkApi = VkApi.getInstance();

    @Override
    public String getCommandName() {
        return "!updateChat";
    }

    @Override
    public void execute(JsonArray params) {
        //ToDo Получен не тот чат, котороый нужен. ПЕРЕДЕЛАТЬ
        Message message = loggerHelper.createMessage(params);
        Chat chat = loggerHelper.getChat(params);
        if (chat == null) return;

        String[] words = message.getMessageText().split(" ");
        if (words.length != 2) return;

        Long conferenceId = Long.valueOf(words[1]);
        if (!vkApi.isConference(conferenceId)) return;
        conferenceId = conferenceId - 2000000000;
        JsonElement element = vkApi.getInstance().executeMethod(VkMethods.MESSAGES_GET_CHAT.getName(),
                Map.entry("chat_id", String.valueOf(conferenceId)),
                Map.entry("access_token", VkApi.getInstance().getSettings().getToken()),
                Map.entry("v", VkApi.getInstance().getSettings().getVersion())
        );

        JsonElement responseElement = element.getAsJsonObject().get("response");
        if (responseElement == null) return;
        chat.setName(responseElement.getAsJsonObject().get("title").getAsString());
        JsonArray users = responseElement.getAsJsonObject().get("users").getAsJsonArray();
        for (JsonElement user : users) {
            if (!chat.getUserIdList().contains(user.getAsLong())) {
                chat.getUserIdList().add(user.getAsLong());
            }
        }
    }
}
