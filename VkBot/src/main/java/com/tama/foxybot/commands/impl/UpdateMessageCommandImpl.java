package com.tama.foxybot.commands.impl;

import com.google.gson.JsonArray;
import com.tama.foxybot.commands.BaseCommand;
import com.tama.foxybot.commands.LoggerCommand;
import com.tama.foxybot.exceptions.LoggerUpdateException;
import com.tama.foxybot.helpers.LoggerCommandHelper;
import com.tama.foxybot.model.Chat;
import com.tama.foxybot.model.Message;
import com.tama.foxybot.service.ChatService;
import com.tama.foxybot.service.MessageService;
import com.tama.foxybot.service.impl.ChatServiceImpl;
import com.tama.foxybot.service.impl.MessageServiceImpl;

public class UpdateMessageCommandImpl extends BaseCommand implements LoggerCommand {
    ChatService chatService = new ChatServiceImpl();
    MessageService messageService = new MessageServiceImpl();

    private LoggerCommandHelper loggerHelper = new LoggerCommandHelper();

    @Override
    public String getCommandName() {
        return "UpdateMessage";
    }

    @Override
    public void execute(JsonArray params) {
        Chat chat = loggerHelper.getChat(params);
        if (chat == null) {
            throw new LoggerUpdateException(String.format("Чат для параметров: %s не найден", params));
        }
        Message oldMessage = loggerHelper.getMessage(params);
        if (oldMessage == null) {
            throw new LoggerUpdateException(String.format("Сообщение для параметров: %s не найдено", params));
        }
        Message newMessage = loggerHelper.createMessage(params);
        newMessage.getHistory().add(oldMessage);

        chat.getMessages().put(oldMessage.getIdMessage(), newMessage);
    }
}
