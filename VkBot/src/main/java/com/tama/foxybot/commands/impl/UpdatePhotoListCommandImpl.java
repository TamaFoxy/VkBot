package com.tama.foxybot.commands.impl;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.tama.foxyapi.VkApi;
import com.tama.foxyapi.VkMethods;
import com.tama.foxybot.VkBot;
import com.tama.foxybot.commands.BaseCommand;
import com.tama.foxybot.commands.UserCommand;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class UpdatePhotoListCommandImpl extends BaseCommand implements UserCommand {
    //ToDo вынести эту логику в отдельные сервисы с нормальными входными параметрами, а тут оставить только вычленение этих параметров.
    @Override
    public String getCommandName() {
        return "!updatePhoto";
    }

    @Override
    public void execute(JsonArray params) {
        String albomOfMessagesPhoto = VkBot.getSettingsBot().getAlbomOfMessagesPhoto();
        String[] albomInfo = albomOfMessagesPhoto.split("_");
        if (albomInfo.length != 2) {
            return;
        }
        String owner_id = albomInfo[0];
        String albom_id = albomInfo[1];

        JsonElement element = VkApi.getInstance().executeMethod(VkMethods.PHOTOS_GET.getName(),
                Map.entry("owner_id", owner_id),
                Map.entry("album_id", albom_id),
                Map.entry("offset", "0"),
                Map.entry("count", "1000"),
                Map.entry("access_token", VkApi.getInstance().getSettings().getToken()),
                Map.entry("v", VkApi.getInstance().getSettings().getVersion())
        );

        JsonElement response = element.getAsJsonObject().get("response");
        if (response == null) {
            return;
        }
        JsonArray photos = response.getAsJsonObject().get("items").getAsJsonArray();
        if (photos.size() == 0) {
            return;
        }
        VkBot.getChatList().clear();
        for (JsonElement photo : photos) {
            String photoId = photo.getAsJsonObject().get("id").getAsString();
            String text = photo.getAsJsonObject().get("text").getAsString();
            if ("".equals(text)) {
                continue;
            }

            String fullPhotoName = getFullPhotoName(owner_id, photoId);
            List<String> chatPhotoList = VkBot.getPhotoToCommand().get(text);
            if (chatPhotoList == null) {
                VkBot.getPhotoToCommand().put(text, new ArrayList<>());
                chatPhotoList = VkBot.getPhotoToCommand().get(text);
            }
            if (!chatPhotoList.contains(fullPhotoName)) {
                chatPhotoList.add(fullPhotoName);
            }
        }
    }

    private String getFullPhotoName(String ownerId, String photoId) {
        return "photo" + ownerId + "_" + photoId;
    }
}
