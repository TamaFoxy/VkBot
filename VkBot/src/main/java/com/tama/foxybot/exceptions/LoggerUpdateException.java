package com.tama.foxybot.exceptions;

public class LoggerUpdateException extends RuntimeBotException {
    public LoggerUpdateException(String message) {
        super(message);
    }
}
