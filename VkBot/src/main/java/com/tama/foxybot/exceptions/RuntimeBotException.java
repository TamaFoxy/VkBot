package com.tama.foxybot.exceptions;

public class RuntimeBotException extends RuntimeException {
    public RuntimeBotException(String message) {
        super(message);
    }
}
