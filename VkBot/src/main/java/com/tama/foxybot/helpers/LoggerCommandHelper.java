package com.tama.foxybot.helpers;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.tama.foxybot.VkBot;
import com.tama.foxybot.model.Chat;
import com.tama.foxybot.model.Message;

//ToDo переделать на синглтон.
public class LoggerCommandHelper {
    private static final int INDEX_OF_ID_MESSAGE = 1;
    private static final int INDEX_OF_ID_CHAT = 3;
    private static final int INDEX_OF_MESSAGE_DATE = 4;
    private static final int INDEX_OF_MESSAGE_TEXT = 5;
    private static final int INDEX_OF_MESSAGE_SENDER = 6;

    public Chat getChat(JsonArray params) {
        long idChat = params.get(INDEX_OF_ID_CHAT).getAsLong();
        if (VkBot.getChatList().containsKey(idChat)) {
            return VkBot.getChatList().get(idChat);
        }

        return null;
    }

    public Chat createChat(JsonArray params) {
        long idChat = params.get(INDEX_OF_ID_CHAT).getAsLong();
        Chat chat = new Chat();
        chat.setId(idChat);
        VkBot.getChatList().put(idChat, chat);
        return chat;
    }

    public Message getMessage(JsonArray params) {
        Long idMessage = params.get(INDEX_OF_ID_MESSAGE).getAsLong();
        Long idChat = params.get(INDEX_OF_ID_CHAT).getAsLong();
        if (VkBot.getChatList().containsKey(idChat)) {
            Chat chat = VkBot.getChatList().get(idChat);
            if (chat.getMessages().containsKey(idMessage)) {
                return chat.getMessages().get(idMessage);
            }
        }

        return null;
    }

    public Message createMessage(JsonArray params) {
        Long idMessage = params.get(INDEX_OF_ID_MESSAGE).getAsLong();
        Long idChat = params.get(INDEX_OF_ID_CHAT).getAsLong();
        Long dateMessage = params.get(INDEX_OF_MESSAGE_DATE).getAsLong();
        String textMessage = params.get(INDEX_OF_MESSAGE_TEXT).getAsString();
        Long idFrom;
        JsonElement fromElement = params.get(INDEX_OF_MESSAGE_SENDER).getAsJsonObject().get("from");
        if (fromElement == null) {
            idFrom = idChat;
        } else {
            idFrom = fromElement.getAsLong();
        }
        Message message = new Message();
        message.setMessageText(textMessage);
        message.setIdUser(idFrom);
        message.setIdMessage(idMessage);
        message.setDate(dateMessage);
        return message;
    }
}
