package com.tama.foxybot.model;

import java.io.Serializable;

public class Attachment implements Serializable {
    private Long id;
    private AttachmentType type;
    private String linkToAttachment;

    public Attachment(Long id, AttachmentType type, String linkToAttachment) {
        this.id = id;
        this.type = type;
        this.linkToAttachment = linkToAttachment;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AttachmentType getType() {
        return type;
    }

    public void setType(AttachmentType type) {
        this.type = type;
    }

    public String getLinkToAttachment() {
        return linkToAttachment;
    }

    public void setLinkToAttachment(String linkToAttachment) {
        this.linkToAttachment = linkToAttachment;
    }
}
