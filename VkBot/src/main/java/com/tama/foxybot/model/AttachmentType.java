package com.tama.foxybot.model;

import java.io.Serializable;

public enum AttachmentType implements Serializable {
    AUDIO("audio"),
    PHOTO("photo"),
    VIDEO("video"),
    DOCUMENT("document"),
    GRAFFITI("graffiti");

    AttachmentType(String type) {
        this.type = type;
    }

    private String type;

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return "AttachmentType{" +
                "type='" + type + '\'' +
                '}';
    }
}
