package com.tama.foxybot.service;

import com.tama.foxybot.model.Chat;

public interface ChatService {
    Chat getChatById(Long id);

    Chat getChatByName(String name);

    void createChat(long id);

    void createChat(Chat chat);
}
