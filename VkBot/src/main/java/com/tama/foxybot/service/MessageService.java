package com.tama.foxybot.service;

import com.tama.foxybot.model.Message;

public interface MessageService {
    Message getMessageByID(Long idMessage);

    Message getMessageByIdFromChat(Long idChat, Long idMessage);

    void createMessageInChat(Long idChat, Long idUser, Long idMessage, String text);

    void createMessageInChat(Long idChat, Message message);

    void updateMessageText(Long idChat, Message message);
}
