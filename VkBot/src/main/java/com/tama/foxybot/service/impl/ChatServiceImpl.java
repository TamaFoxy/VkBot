package com.tama.foxybot.service.impl;

import com.tama.foxybot.VkBot;
import com.tama.foxybot.model.Chat;
import com.tama.foxybot.service.ChatService;
import org.jetbrains.annotations.NotNull;

public class ChatServiceImpl implements ChatService {
    @Override
    @NotNull
    public Chat getChatById(Long id) {
        return VkBot.getChatList().get(id);
    }

    @Override
    public Chat getChatByName(String name) {
        return null;
    }

    @Override
    public void createChat(long id) {
        if (VkBot.getChatList().containsKey(id)) {
            return;
        }

        Chat chat = new Chat();
        chat.setId(id);
        VkBot.getChatList().put(id, chat);
    }

    @Override
    public void createChat(Chat chat) {
        if (VkBot.getChatList().containsKey(chat.getId())) {
            return;
        }

        VkBot.getChatList().put(chat.getId(), chat);
    }
}