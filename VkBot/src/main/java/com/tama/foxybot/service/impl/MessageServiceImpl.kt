package com.tama.foxybot.service.impl

import com.tama.foxybot.model.Message
import com.tama.foxybot.service.ChatService
import com.tama.foxybot.service.MessageService

class MessageServiceImpl : MessageService {
    private val chatService: ChatService = ChatServiceImpl()

    override fun getMessageByID(idMessage: Long?): Message? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getMessageByIdFromChat(idChat: Long?, idMessage: Long?): Message? {
        idChat ?: return null
        idMessage ?: return null

        val chatById = chatService.getChatById(idChat) ?: return null
        return chatById.messages[idMessage]
    }

    override fun createMessageInChat(idChat: Long?, idUser: Long?, idMessage: Long?, text: String?) {
        idChat ?: return
        idUser ?: return
        idMessage ?: return
        text ?: return

        val chat = chatService.getChatById(idChat) ?: return
        val message = Message()
        message.idMessage = idMessage
        message.idUser = idUser
        message.messageText = text
        chat.messages[idMessage] = message
    }

    override fun createMessageInChat(idChat: Long?, message: Message?) {
        idChat ?: return
        message ?: return
        message.idMessage ?: return
        message.idUser ?: return
        message.messageText ?: return

        val chat = chatService.getChatById(idChat) ?: return
        if (chat.messages.containsKey(message.idMessage)) return
        chat.messages[message.idMessage] = message
    }

    override fun updateMessageText(idChat: Long?, message: Message?) {
        idChat ?: return
        message ?: return
        message.idMessage ?: return
        message.idUser ?: return
        message.messageText ?: return

        val chat = chatService.getChatById(idChat) ?: return
        if (!chat.messages.containsKey(idChat)) return
        chat.messages[message.idMessage] = message
    }
}